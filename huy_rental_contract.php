<?php
	session_start();
	$res_num=$_SESSION['res_num'];
	$clerk=$_SESSION['clerk'];
	
	#need clerk name, deposit held, estimated rental

	require('DBConnection/connection.php');
	
	$ccNum=$_POST["ccNum"];
	$expDateNum=$_POST["expDate"];
	
	#CALC DEPOSIT HELD
	$sql='SELECT SUM(Deposit_amt) from (SELECT `tool`.Tool_id, `tool`.Rental_price, `tool`.Deposit_amt  
		  FROM `reservation_line_item` 
		  JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
		  WHERE FK_Reservation_num='.$res_num.') temp_table';
	$result=$connection->query($sql);
	$row = $result->fetch_assoc();
	$dep_held=$row['SUM(Deposit_amt)'];
	$dep_held='$'.$dep_held;
	
	#CALC ESTIMATED RENTAL
	$sql='SELECT SUM(Rental_price) from (SELECT `tool`.Tool_id, `tool`.Rental_price, `tool`.Deposit_amt 
	FROM `reservation_line_item` 
	JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
	WHERE FK_Reservation_num='.$res_num.') temp_table';
	$result=$connection->query($sql);
	$row = $result->fetch_assoc();
	$rental_cost=$row['SUM(Rental_price)'];
	
	#GET NUMBER OF DAYS
	$sql='SELECT `reservation_header`.Start_date, `reservation_header`.End_Fodate
			FROM `reservation_header`
			WHERE Reservation_num='.$res_num.';';
	$result=$connection->query($sql);
	while($row = $result->fetch_assoc()) 
	{
		$start_date=$row['Start_date'];
		$end_date=$row['End_Fodate'];
	}
	$end_date=date_create($end_date);
	$start_date=date_create($start_date);
	$num_days=date_diff($end_date,$start_date);
	$num_days=($num_days->format('%d')+1); #GET TOTAL NUMBER OF DAYS RESERVED (FENCE POST +1)
	$rental_cost=$rental_cost*$num_days;#MULT NUM DAYS BY RENTAL TOTAL
	$rental_cost='$'.$rental_cost.'.00';

	#UPDATE CUSTOMER CC TO DB
	$sql='UPDATE `handy_tool`.`reservation_header` SET `Credit_card_num` ='.$ccNum.' WHERE `reservation_header`.`Reservation_num` ='. $res_num .';';
	$connection->query($sql);
	
	$sql='SELECT FK_Customer_login_id, Start_date, End_Fodate FROM `handy_tool`.`reservation_header` WHERE Reservation_num='. $res_num .';';
	$result=$connection->query($sql);
	$row = $result->fetch_assoc();
	$customer=$row['FK_Customer_login_id'];
	$start_date=$row['Start_date'];
	$end_date=$row['End_Fodate'];
	
	$sql='SELECT `tool`.Tool_id, `tool`.Abbr_description 
			FROM `reservation_line_item` 
			JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
			WHERE FK_Reservation_num='.$res_num.'';
	$result=$connection->query($sql);
	
	echo '
		<html>
		<body>
			<title>Rental Contract</title>
			<h1><i>HANDYMAN TOOLS RENTAL CONTRACT</i></h1>
			<table>
			<tr>
				<td><b>Reservation Number: </b></td> 
				<td>' .$res_num. '</td>
				<td></td><td></td><td></td><td></td><td></td><td></td>
				<td><b>Clerk on Duty: </b></td> 
				<td>'.$clerk.'</td>
			</tr>
			<tr>
				<td><b>Customer Name: </b></td> 
				<td>' .$customer. '</td>
				<td></td><td></td><td></td><td></td><td></td><td></td>
				<td><b>Credit Card #:</b></td> 
				<td>' .$ccNum. '</td>
			</tr>
			<tr>
				<td><b>Start Date: </b></td> 
				<td>' .$start_date. '</td>
				<td></td><td></td><td></td><td></td><td></td><td></td>
				<td><b>End Date:</b></td> 
				<td>' .$end_date. '</td>
			</tr>
			</table>
			
			<br>
			<b>Tools Rented:</b>';
			
	echo "<table>";
	while($row = $result->fetch_assoc())
	{
		echo "<tr>";
		echo "<td>".$row['Tool_id']."</td>";
		echo "<td></td><td></td><td></td><td></td>";
		echo "<td>".$row['Abbr_description']."</td>";
		echo "</tr>";
	}
	echo "</table>";
			
	echo '<br>

			<table>
				<tr>
					<td><b>Deposit Held:</b></td>
					<td></td><td></td><td></td><td></td><td></td><td></td>
					<td>'.$dep_held.'</td>
				</tr>
				<tr>
					<td><b>Estimated Rental:</b></td>
					<td></td><td></td><td></td><td></td><td></td><td></td>
					<td>'.$rental_cost.'</td>
				</tr>
			</table>
			
			<br><br><br><br>
			<b>Signature: ___________________________________________________</b>
			
		</body>
		</html>';
?>
