<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require('DBConnection/connection.php');

$tool_type = $_POST['tool_type'];
//$tool_inventory = $POST['tool_inventory'];
$Start_date = $_POST['start_date'];
$End_date = $_POST['end_date'];


//get all the tools pertaining to selected category
$tools = $connection->query("SELECT * from Tool where Category = '$tool_type'");

//keep track of id
$id_array = array();

while($tool = $tools->fetch_assoc()){
	// print_r($tool);
	$tool_id = $tool['Tool_id'];
	//check if reservation or service order is not found
	$hasBeenReserved = $connection->query("SELECT * from Reservation_line_item where FK_Tool_id = $tool_id");
	$hasBeenServiced = $connection->query("SELECT * from Service_order where FK_Tool_id = $tool_id");
	if($hasBeenReserved->num_rows == 0 && $hasBeenServiced->num_rows == 0){
		$series['id'] = $tool_id;
		array_push($id_array, $series);
	}else{
		//check reservation header
		switch ($tool_type) {
			case 'handtool':
				$sql = "SELECT 
				              ht.FK_Hand_tool_id
				            , tool.Abbr_description
				           
				             from Tool tool, Hand_tool ht 
							
							left join Reservation_line_item rli
							on ht.FK_Hand_tool_id = rli.FK_Tool_id
											
							left join Reservation_header rh 
							on rli.FK_Reservation_num = rh.Reservation_num
							
							WHERE
							
							tool.Tool_id  = ht.FK_Hand_tool_id  
							
							and NOT (('$Start_date' >= rh.Start_date AND '$Start_date' <= rh.End_date)
							OR ('$End_date' >= rh.Start_date AND '$End_date' <= rh.End_date))";
				break;
			case 'constructiontool':
				$sql = "SELECT
				           
				              ct.FK_Construct_tool_id
				            , tool.Abbr_description
				           
				           from Tool tool, Construction_tool ct 
				           
							left join Reservation_line_item rli
							on ct.FK_Construct_tool_id = rli.FK_Tool_id
							
							left join Reservation_header rh 
							on rli.FK_Reservation_num = rh.Reservation_num
							
							WHERE
							
							tool.Tool_id  = ct.FK_Construct_tool_id
							
							and NOT (('$Start_date' >= rh.Start_date AND '$Start_date' <= rh.End_date)
							OR ('$End_date' >= rh.Start_date AND '$End_date' <= rh.End_date))";
				break;
			case 'powertool':
				$sql = "SELECT 
				
				            pt.FK_Power_tool_id
				            , tool.Abbr_description
				
				            from  Tool tool , Power_tool pt
							
							left join Reservation_line_item rli 
							on pt.FK_Power_tool_id = rli.FK_Tool_id
							
							left join Reservation_header rh 
							on rli.FK_Reservation_num = rh.Reservation_num
							
							WHERE 
							
							tool.Tool_id  = pt.FK_Power_tool_id
							  
							and NOT (('$Start_date' >= rh.Start_date AND '$Start_date' <= rh.End_date)
							OR ('$End_date' >= rh.Start_date AND '$End_date' <= rh.End_date))";
				break;
			
			default:
				echo 'Tool type unknown!';
				break;
		}
		$toolsavailable = $connection->query($sql);
				
		if ($toolsavailable ->num_rows > 0) {
			//output data for each row
			while($row = $connection->fetch_assoc($sql))
			{
				
			   foreach ($id_array as $row){
				echo "id: $row[$id]. ";
				echo "Description: $row[$Abbr_description].";	
			   }
			 } 
		  }
			else {
				echo "No tools available";
			}
	
	
						
?>		


