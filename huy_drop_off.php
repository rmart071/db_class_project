<?php
	#need clerk name, check to see how res number is read in
	#rental price, deposit held, total

	require('DBConnection/connection.php');
	
	session_start();
	$res_num=$_SESSION['res_num'];
	
	#GET DROP OFF RECEIPT INFO
	$sql='SELECT FK_Customer_login_id, Start_date, End_Fodate, Credit_card_num FROM `handy_tool`.`reservation_header` WHERE Reservation_num='. $res_num .';';
	$result=$connection->query($sql);
	$row = $result->fetch_assoc();
	$customer=$row['FK_Customer_login_id'];
	$start_date=$row['Start_date'];
	$end_date=$row['End_Fodate'];
	$ccNum=$row['Credit_card_num'];
	
	#NOT SURE WHY I WROTE THIS IF I NEVER USED IT?!
	$sql='SELECT `tool`.Tool_id, `tool`.Abbr_description 
			FROM `reservation_line_item` 
			JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
			WHERE FK_Reservation_num='.$res_num.'';
	$result=$connection->query($sql);
	
	echo '
		<html>
		<body>
			<title>Rental Receipt</title>
			<h1><i>HANDYMAN TOOLS RECEIPT</i></h1>
			<table>
			<tr>
				<td><b>Reservation Number: </b></td> 
				<td>' .$res_num. '</td>
				<td></td><td></td><td></td><td></td><td></td><td></td>
				<td><b>Clerk on Duty: </b></td> 
				<td>Clerk Name</td>
			</tr>
			<tr>
				<td><b>Customer Name: </b></td> 
				<td>' .$customer. '</td>
				<td></td><td></td><td></td><td></td><td></td><td></td>
				<td><b>Credit Card #:</b></td> 
				<td>' .$ccNum. '</td>
			</tr>
			<tr>
				<td><b>Start Date: </b></td> 
				<td>' .$start_date. '</td>
				<td></td><td></td><td></td><td></td><td></td><td></td>
				<td><b>End Date:</b></td> 
				<td>' .$end_date. '</td>
			</tr>
			</table>
			
			<br><br>

			<table>
				<tr>
					<td><b>Rental Price:</b></td>
					<td></td><td></td><td></td><td></td><td></td><td></td>
					<td>$xxx.xx</td>
				</tr>
				<tr>
					<td><b>Deposit Held:</b></td>
					<td></td><td></td><td></td><td></td><td></td><td></td>
					<td>$xxx.xx</td>
				</tr>
			</table>
			
			<br>
			<b>------------------------------------------------------------------------------------------------------------------------------</b>
			<br><br>
			
			<table>
				<tr>
					<td><b>Total:</b></td>
					<td></td><td></td><td></td><td></td><td></td><td></td>
					<td>$xxx.xx</td>
				</tr>
			</table>
			
		</body>
		</html>';
?>
