<?php 
//session_start()

error_reporting(E_ALL);
ini_set('display_errors', 1);

class functions {


	// echo $toolID;
	function viewDetails(){

		require('../DBConnection/connection.php');

		$tool_id = $_GET['toolID'];
		$sql = "SELECT * from Tool where Tool_id = $tool_id";

		$result = $connection->query($sql)->fetch_assoc();
		if($result['Category'] == 'P')
			$type = 'Power Tool';
		elseif($result['Category'] == 'C')
			$type = 'Construction Tool';
		elseif($result['Category'] == 'H')
			$type = 'Hand Tool';
		else
			$type = 'Unknown Type';

		echo '<table class="table table-hover">';
		echo '<tbody>
			<tr>
				<td>Abbreviated Description: </td><td>'.$result['Abbr_description'].'</td>
			</tr>
			<tr>
				<td>Full Description: </td><td>'.$result['Long_description'].'</td>
			</tr>
			<tr>
				<td>Rental Price: </td><td>$'.$result['Rental_price'].'</td>
			</tr>
			<tr>
				<td>Deposit Amount: </td><td>$'.$result['Deposit_amt'].'</td>
			</tr>
			<tr>
				<td>Original Purchase Price: </td><td>$'.$result['Original_purchase_price'].'</td>
			</tr>
			<tr>
				<td>Tool Type: </td><td>'.$type.'</td>
			</tr>
				</tbody>';
		echo '</table>';


		if($result['Category'] == 'P'){
			$accessory_sql = "SELECT * FROM Accessory";
			

			echo '<hr><h2>Tool Accessories</h2>';
			echo '<table class="table table-hover" >';
			echo '<thead>
					<th>Name</th>
					<th>Quantity</th>
			</thead>';
			echo '<tbody>';
			$result = $connection->query($accessory_sql);
			while($row = $result->fetch_assoc()){
			echo '	
				<tr>
					<td>'.$row['Name'].'</td>
					<td>'.$row['Quantity'].'</td>
				</tr>';
			}


			echo '</tbody>
				</table>';

		}


		// echo 'ohh shit';
	}

	function createNewCustomer()
	{
		require('../DBConnection/connection.php');
		
		$user=$_GET['custLogin'];
		$pass=$_GET['custPass'];
		$c_pass=$_GET['custCpass'];
		$fname=$_GET['custFname'];
		$lname=$_GET['custLname'];
		$hphone=$_GET['custHomePhone'];
		$wphone=$_GET['custWorkPhone'];
		$street=$_GET['custStreet'];
		$city=$_GET['custCity'];
		$state=$_GET['custState'];
		$zip=$_GET['custZip'];
		
		$user = preg_replace('/\s+/', '', $user); //STRIP WHITESPACES

		if($pass!= $c_pass) #passwords don't match, refresh same page
		{
			echo 'Password Mismatch';
			return;
		}

		if($user == '' || $user == NULL)
		{
			echo 'Emptykey';
			return;
		}

		$sql = "SELECT * FROM `customer` WHERE Login_id='".$user."'";
		$result = $connection->query($sql);
		if($result->num_rows > 0) //USER ALREADY EXISTS
		{
			echo 'Keyclash';
			return;
		}		

		$sql="INSERT INTO `handy_tool`.`customer`(`Login_id`, `Password`, `First_name`, `Last_name`, `Home_phone`, `Work_phone`, `Street`, `City`, `State`, `Zip`) 
		VALUES ('$user', '$pass', '$fname', '$lname', '$hphone', '$wphone', '$street', '$city', '$state', '$zip')";
			
		if($connection->query($sql) === TRUE) 
		{
			echo "New record created successfully";
			session_start();
			$_SESSION['login_id'] = $user;
		} 
		else 
		{
			echo "Error: " . $sql . "<br>" . $connection->error;
		}
	}
	
	function createNewEmp()
	{
		require('../DBConnection/connection.php');

		$emp=$_GET['empLogin'];
		$epass=$_GET['empPass'];
		$cpass=$_GET['empCpass'];
		$fn=$_GET['empFName'];
		$ln=$_GET['empLName'];
		$socsec=$_GET['empSS'];
		
		$emp = preg_replace('/\s+/', '', $emp); //STRIP WHITESPACES

		if($epass!=$cpass) #passwords don't match, refresh same page
		{
			echo 'Password Mismatch';
			return;
		}
		
		if($emp == NULL || $emp == '')
		{
			echo 'Emptykey';
			return;
		}
		
		$sql = "SELECT * FROM `employee` WHERE Login_id='".$emp."'";
		$result = $connection->query($sql);
		if($result->num_rows > 0) //EMP ALREADY EXISTS
		{
			echo 'Keyclash';
			return;
		}
		
		if($socsec=='' || $socsec==NULL) #invalid social check, refresh same page
		{
			echo 'SS Error';
			return;
		} 
		else
		{
			$sql="INSERT INTO `handy_tool`.`employee`(`Login_id`, `Password`, `First_name`, `Last_name`, `Ssn`) 
						VALUES ('$emp', '$epass', '$fn', '$ln', '$socsec')";
					
			if($connection->query($sql) === TRUE) 
			{
				echo "New emp record created successfully";
				session_start();
				$_SESSION['login_id'] = $emp;
			} 
			else 
			{
				echo "Error: ".$sql."<br>".$connection->error;
			}
		}
	}
	
	function getAvailableTools(){
		require('../DBConnection/connection.php');
		$tool_type = $_GET['tool_type'];
		$Start_date = $_GET['Start_date'];
		$End_date = $_GET['End_date'];

		//get all the tools pertaining to selected category
		$tools = $connection->query("SELECT t.* from Tool t where t.Category = '$tool_type' AND t.Sold_sw=0 and NOT EXISTS(SELECT * from service_order s where s.FK_Tool_id = t.Tool_id and (isnull(s.Start_date) or
							((('$Start_date' <= s.End_date and '$End_date' >= s.Start_date)))));");

		$id_array = array();

		while($tool = $tools->fetch_assoc()){


			// print_r($tool);
			$tool_id = $tool['Tool_id'];
			// check if reservation or service order is not found
			$hasBeenReserved = $connection->query("SELECT * from Reservation_line_item where FK_Tool_id = $tool_id");
			$hasBeenServiced = $connection->query("SELECT * from Service_order where FK_Tool_id = $tool_id");
			if($hasBeenReserved->num_rows == 0 && $hasBeenServiced->num_rows == 0){


				switch ($tool_type) {
				case 'H':
				$column = 'FK_Hand_tool_id';
				$sql = "SELECT 
							  ht.FK_Hand_tool_id
							, tool.*
							from Tool tool, Hand_tool ht 
				WHERE
				tool.Tool_id  = ht.FK_Hand_tool_id 
				and tool.Tool_id = $tool_id ";
				break;
				case 'C':
				$column = 'FK_Construct_tool_id';
				$sql = "SELECT
							  ct.FK_Construct_tool_id
							, tool.*
						  from Tool tool, Construction_tool ct
				WHERE
				tool.Tool_id  = ct.FK_Construct_tool_id
				and tool.Tool_id = $tool_id";
				break;
				case 'P':
				$column = 'FK_Power_tool_id';
				$sql = "SELECT 
							pt.FK_Power_tool_id
							, tool.*
							from  Tool tool , Power_tool pt
				WHERE 
				tool.Tool_id  = pt.FK_Power_tool_id
				and tool.Tool_id = $tool_id";
				break;

				default:
				echo 'Tool type unknown!';
				break;
				}
				$toolsavailable = $connection->query($sql);
				while($row = $toolsavailable->fetch_assoc()){
				$series['id'] = $tool_id;

				$series['description'] = $row['Abbr_description'];
				$series['deposit'] = $row['Deposit_amt'];
				$series['rental_price'] = $row['Rental_price'];
				array_push($id_array, $series);
				}
			}else{
				//check reservation header
				switch ($tool_type) {
					case 'H':
						$column = 'FK_Hand_tool_id';
						$sql = "SELECT  tool.*, ht.*
							from  Hand_tool ht
							join Tool tool on tool.Tool_id = ht.FK_Hand_Tool_id
														left join Reservation_line_item rli
														on tool.Tool_id = rli.FK_Tool_id
														left join Reservation_header rh 
														on rli.FK_Reservation_num = rh.Reservation_num
														WHERE
														tool.Tool_id = $tool_id 
														and (
														(NOT (('$Start_date' >= rh.Start_date AND '$Start_date' <= rh.End_date)
														OR ('$End_date' >= rh.Start_date AND '$End_date' <= rh.End_date))))";
						break;
					case 'C':
						$column = 'FK_Construct_tool_id';
						$sql = "SELECT  tool.*, ct.*
							from  Construction_tool ct
							join Tool tool on tool.Tool_id = ct.FK_Construct_tool_id          
								          
								left join Reservation_line_item rli
								on ct.FK_Construct_tool_id = rli.FK_Tool_id

								left join Reservation_header rh 
								on rli.FK_Reservation_num = rh.Reservation_num

								WHERE

								tool.Tool_id  = ct.FK_Construct_tool_id
								and tool.Tool_id = $tool_id 
														and (
														(NOT (('$Start_date' >= rh.Start_date AND '$Start_date' <= rh.End_date)
														OR ('$End_date' >= rh.Start_date AND '$End_date' <= rh.End_date))))";
						break;
						case 'P':
						$column = 'FK_Power_tool_id';
						$sql = "SELECT  tool.*, pt.*
							from  Power_tool pt
							join Tool tool on tool.Tool_id = pt.FK_Power_tool_id 

							left join Reservation_line_item rli 
							on pt.FK_Power_tool_id = rli.FK_Tool_id

							left join Reservation_header rh 
							on rli.FK_Reservation_num = rh.Reservation_num

							WHERE 

							tool.Tool_id  = pt.FK_Power_tool_id
							and tool.Tool_id = $tool_id 
														and (
														(NOT (('$Start_date' >= rh.Start_date AND '$Start_date' <= rh.End_date)
														OR ('$End_date' >= rh.Start_date AND '$End_date' <= rh.End_date))))";
						break;
					
					default:
						echo 'Tool type unknown!';
						break;
				}
				$toolsavailable = $connection->query($sql);
						
				if ($toolsavailable->num_rows > 0) {
					//output data for each row
					// $connection->query($sql)->fetch_assoc()

					while($row = $toolsavailable->fetch_assoc())
					{

					//	$series['id'] = $row[$column];
					    $series['id'] = $tool_id;
						$series['description'] = $row['Abbr_description'];
                        $series['deposit']     = $row['Deposit_amt'];
						$series['rental_price'] = $row['Rental_price'];

						 array_push($id_array, $series);
					 } 
				  }
					// else {
					// 	$series['id'] = '0';
					// 	$series['description'] = 'No Tools Available';
					// 	$series['deposit']     = 'No Tools Available';
					// 	$series['rental_price'] = 'No Tools Available';
					// 	array_push($id_array, $series);
					// }
				}
			}
				print json_encode($id_array);
	}

function getCalcTotal(){
	    require('../DBConnection/connection.php');
        session_start();
		
		$start_date = $_GET['start_date'];
		$end_date = $_GET['end_date'];
		$tool_selected = $_GET['tool'];
		$tool_id    = $_GET['tool_id'];
		
		$sql="SELECT SUM(Deposit_amt), SUM(Rental_price) from (SELECT `tool`.Tool_id, `tool`.Rental_price, 
		`tool`.Deposit_amt FROM `tool` WHERE `tool`.Abbr_description = '$tool_selected'  
		 and `tool`.Tool_id = '$tool_id' ) temp_table";


		$result=$connection->query($sql);
		while($row = $result->fetch_assoc()) {
               $deposit_amt=$row['SUM(Deposit_amt)'];
               $rental_cost= $row['SUM(Rental_price)'];
		}  
    

         $_SESSION['deposit_amt'] = $deposit_amt; 
         $_SESSION['rental_cost'] = $rental_cost;
         $_SESSION['start_date'] = $start_date ;
         $_SESSION['end_date'] = $end_date ;
         $_SESSION['tool'] = $tool_selected ;
         $_SESSION['tool_id'] = $tool_id ;
		 
    }

    function displayReservationData(){
    session_start();
	$final = array();
	$series['startDate'] = $_SESSION['start_date'];
	$series['endDate'] = $_SESSION['end_date'];
	$series['rentalCost'] = $_SESSION['rental_cost'];
	$series['depositAmount'] = $_SESSION['deposit_amt'];
	$series['tool'] = $_SESSION['tool'];
	
	array_push($final, $series);
			
	print json_encode($final);	
	}
	
	function reservationFinal(){
session_start();
require('../DBConnection/connection.php');
   
$start_date   = $_GET['start_date'];
$end_date = $_GET['end_date'];
$rental_cost = $_GET['rental_cost'];
$deposit_amt = $_GET['dep_amt'];
$tool =  $_GET['tool'];	
$custID = $_SESSION['login_id'];
$tool_id  = $_SESSION['tool_id'];

$sql = "INSERT INTO `handy_tool`.`Reservation_header` (`Start_date`, `End_date`, `FK_Customer_login_id`) VALUES ('$start_date' , '$end_date', '$custID')";
       
if($connection->query($sql)){

$result = $connection->query("SELECT `Reservation_num` FROM `Reservation_header` WHERE Start_date = '$start_date' and End_date = '$end_date' and `FK_Customer_login_id` = '$custID'");

$row = $result->fetch_assoc();
$_SESSION['reservation_num'] = $row['Reservation_num'];
$reservation_num = $_SESSION['reservation_num'];

}


//$reservation_num = $_SESSION['reservation_num'];
$sql2 = "INSERT INTO `handy_tool`.`Reservation_line_item` (`FK_Reservation_num`, `Sequence_num`, `FK_Tool_id`) VALUES ('$reservation_num' , 3, '$tool_id')";

if($connection->query($sql2)){

$result = $connection->query("SELECT `FK_Reservation_num` FROM `Reservation_line_item` WHERE FK_Reservation_num = '$reservation_num' and FK_Tool_id = '$tool_id'");
$row = $result->fetch_assoc();

}


echo 'Update complete';

}
	
	function displayFinalReservationData(){
    session_start();
	$final = array();
	$series['startDate'] = $_SESSION['start_date'];
	$series['endDate'] = $_SESSION['end_date'];
	$series['rentalCost'] = $_SESSION['rental_cost'];
	$series['depositAmount'] = $_SESSION['deposit_amt'];
	$series['tool'] = $_SESSION['tool'];
	$series['reservation_num'] = $_SESSION['reservation_num'];
	
	array_push($final, $series);
			
	print json_encode($final);	
	}
	

	function insertNewTool(){
		require('../DBConnection/connection.php');
		$abbr_description = $_GET['abbr_description'];
		$tool_type = $_GET['tool_type'];
		$purchase_price = $_GET['purchase_price'];
		$rental_price = $_GET['rental_price'];
		$deposit_amount = $_GET['deposit_amount'];
		$full_description = $_GET['full_description'];
		$name_array = $_GET['name_array'];
		$qty_array = $_GET['qty_array'];
		$value9 = 'Power';
		$value10 = 'Medium';
		$value11 = '6 LB';


		if ($tool_type == "Construction")
		  {
		    
		    $temp_value = 'C';
		  }

		if ($tool_type == "Power")
		  {
		  
		    $temp_value = 'P';
		  }
		  
		if ($tool_type == "Hand")
		  {
		     
		    $temp_value = 'H';
		  }

		  $sql = "INSERT INTO Tool     
		  		(Abbr_description,
			      Long_description,
			      Sold_sw,				
			      Rental_price,
			      Deposit_amt,
			      Original_purchase_price,
			      Category)
		     VALUES ('$abbr_description',
			     '$full_description',
			     '0',	
			     '$rental_price',
			     '$deposit_amount',
			     '$purchase_price',
		             '$temp_value')";

		if(!$connection->query($sql))
					echo 'Error';

		$result = $connection->query("SELECT MAX(Tool_id) as Tool_id FROM Tool");
		$row = $result->fetch_assoc();
		$highest_id = $row['Tool_id'];

		switch ($tool_type) {
			case "Power":
		 

				$sql = "INSERT INTO Power_tool  (FK_Power_tool_id,
		   		       	 			 Power_source)
				      			 VALUES ('$highest_id',
				    	 		  	 '$value9')";

				if(!$connection->query($sql))
					echo 'Error';
		 
				break;

		    
			case "Construction":


				$sql = "INSERT INTO Construction_tool  (FK_Construct_tool_id,
		                                        	       Size)
				              		 VALUES       ('$highest_id',
				    	               		       '$value10')";

				if(!$connection->query($sql))
					echo 'Error';
				break;

			case "Hand":
				$sql = "INSERT INTO Hand_tool  (FK_Hand_tool_id,
				   	                		Weight)
				                    	       VALUES   ('$highest_id',
					    	              		 '$value11')";
				if(!$connection->query($sql))
					echo 'Error';
				break;
		 
		}
				
			    				
		switch ($tool_type) {
			case "Power":
			if(!empty($name_array[0])){
				for($x = 0; $x < sizeof($name_array); $x++){
					$name = $name_array[$x];
					$qty = $qty_array[$x];
					$sql = "INSERT INTO Accessory  (FK_Acc_pow_tool_id,
		                               			 Name,
		   	                      		        Quantity)
			            		 VALUES         ('$highest_id',
			   	        			 '$name',
								 '$qty')";
					if(!$connection->query($sql))
					echo 'Error';
				}
			}
			
				break;

		 }

		 echo 'Update complete';


	}

	function getCustProfile()
	{
		require('../DBConnection/connection.php');
		session_start();
		$custID = $_SESSION['login_id'];
		
		//GET TOP PORTION OF USER PROFILE
		$fname; $lname; $hphone; $wphone;
		$street; $city; $state; $zip;

		$user_array = array();
		$sql="SELECT * FROM customer WHERE customer.Login_id='".$custID."'";
		$result = $connection->query($sql);
		if($result->num_rows > 0)
		{
			$row = $result->fetch_assoc();
			$fname = $row['First_name'];
			$lname = $row['Last_name'];
			$hphone = $row['Home_phone'];
			$wphone = $row['Work_phone'];
			$street = $row['Street'];
			$city = $row['City'];
			$state = $row['State'];
			$zip = $row['Zip'];
		}
		array_push($user_array, $custID,$fname,$lname,$hphone,$wphone,$street,$city,$state,$zip);
		
		//GET RESERVATION HISTORY
		$res_num; $a_desc; $sdate; $edate;
		$rent_price; $dep_amt; $pu_clerk; $do_clerk;
		
		$user_res_history = array();
		
		$sql = "SELECT Reservation_num, Abbr_description, Start_date, End_date, Rental_price, Deposit_amt, FK_Pickup_login_id, FK_Dropoff_login_id
				FROM
				(SELECT * FROM `reservation_header`
				JOIN `reservation_line_item`
				ON `reservation_header`.`Reservation_num` = `reservation_line_item`.`FK_Reservation_num`
				JOIN `tool`
				ON `tool`.`Tool_id` = `reservation_line_item`.`FK_Tool_id`
				WHERE `reservation_header`.`FK_Customer_login_id` = '".$custID."') temp_table";
	
		$result = $connection->query($sql);
		if($result->num_rows > 0)
		{	
			while($row = $result->fetch_assoc())
			{
				$res_num = $row['Reservation_num'];
				$a_desc = $row['Abbr_description'];
				$sdate = $row['Start_date'];
				$edate = $row['End_date'];
				$rent_price = $row['Rental_price'];
				$dep_amt = $row['Deposit_amt'];
				$pu_clerk = $row['FK_Pickup_login_id'];
				$do_clerk = $row['FK_Dropoff_login_id'];
				
				$temp_edate = date_create($edate);
				$temp_sdate = date_create($sdate);
				$num_days = date_diff($temp_edate,$temp_sdate);
				$num_days = ($num_days->format('%d')+1); #GET TOTAL NUMBER OF DAYS RESERVED (FENCE POST +1)
				$rent_price = $rent_price * $num_days;#MULT NUM DAYS BY RENTAL TOTAL
				$rent_price = $rent_price.'.00';
				
				$temp = array();
				array_push($temp, $res_num,$a_desc,$sdate,$edate,$rent_price,
							$dep_amt,$pu_clerk,$do_clerk);
				array_push($user_res_history, $temp);
			}
		}
		
		$final = array();
		
		$final['profile']=$user_array;
		$final['history']=$user_res_history;
		
		print json_encode($final);
	}

	function inventoryReport(){
		require('../DBConnection/connection.php');
		session_start();
		$final = array();
		$div = '';

		$sql = 'SELECT t.Tool_id,t.Abbr_description , datediff(rh.End_date,rh.Start_date)* t.Rental_price as rental_cost,
				IFNULL((datediff(s.End_date,s.Start_date)+t.Original_purchase_price),t.Original_purchase_price) as cost_tool,
				IFNULL((datediff(rh.End_date,rh.Start_date)* t.Rental_price) - (datediff(s.End_date,s.Start_date)+t.Original_purchase_price), t.Rental_price+t.Original_purchase_price ) as total_profit
				from Tool t
				left join Reservation_line_item rli on t.Tool_id = rli.FK_Tool_id 
				left join reservation_header rh on rli.FK_Reservation_num = rh.Reservation_num
				left join Service_order s on t.Tool_id = s.FK_Tool_id
				where t.Sold_sw = 0
				and (month(rh.Start_date) = month(curdate())-1 or isnull(rh.Start_date))
				and (year(rh.Start_date) = year(curdate()) or isnull(rh.Start_date))';

		$result = $connection->query($sql);

		while($row = $result->fetch_assoc()){
			$div .= '<tr>';
			$div .= "<td>".$row['Tool_id']."</td><td>".$row['Abbr_description']."</td><td>".$row['rental_cost']."</td><td>".$row['cost_tool']."</td><td>".$row['total_profit']."</td>";
			$div .= '</tr>';
		}

		echo $div;
	}

	function customerReport(){
		require('../DBConnection/connection.php');
		session_start();
		$final = array();
		$div = '';

		$sql = 'SELECT c.First_name,c.Last_name, c.Login_id, count(*) as num_rental from customer c
				left join reservation_header rh on c.Login_id = rh.FK_Customer_login_id
				where month(rh.Start_date) = month(curdate())-1
				and year(rh.Start_date) = year(curdate())
				group by c.Login_id
				order by num_rental, c.Last_name';

		$result = $connection->query($sql);

		while($row = $result->fetch_assoc()){
			$div .= '<tr>';
			$div .= "<td>".$row['First_name']."</td><td>".$row['Last_name']."</td><td>".$row['Login_id']."</td><td>".$row['num_rental']."</td>";
			$div .= '</tr>';
		}

		echo $div;
	}


	function employeeReport(){
		require('../DBConnection/connection.php');
		session_start();
		$final = array();
		$div = '';

		$sql = 'SELECT e.First_name, e.Last_name,count(FK_Pickup_login_id) as total_pickup, count(FK_Dropoff_login_id) as total_dropoff from employee e
				left join reservation_header rh on e.Login_id = rh.FK_Pickup_login_id
				and month(rh.Start_date) = month(curdate())-1
				and year(rh.Start_date) = year(curdate())
				group by e.Login_id
				order by total_pickup desc, total_dropoff desc';

		$result = $connection->query($sql);

		while($row = $result->fetch_assoc()){
			$div .= '<tr>';
			$div .= "<td>".$row['First_name']."</td><td>".$row['Last_name']."</td><td>".$row['total_pickup']."</td><td>".$row['total_dropoff']."</td>";
			$div .= '</tr>';
		}

		echo $div;
	}
	
	function Login(){
		require('../DBConnection/connection.php');
		$username = $_GET['username'];
		$password = $_GET['password'];
		$userType = $_GET['userType'];

		//check if user name exists
		$checkUsername = $connection->query("SELECT Login_id FROM `$userType` WHERE Login_id='$username'");
		
		if($checkUsername->num_rows > 0){
			$result = $connection->query("SELECT Login_id, Password FROM `$userType` WHERE Login_id='$username' AND Password='$password'");
			if(!($result->num_rows > 0))
				echo 'Failed';
			else{
				session_start();
				echo 'Success';
				$row = $result->fetch_assoc();
				$_SESSION['login_id'] = $row['Login_id'];
			}

		}else
			echo 'Not Exists';		
	}


	function getTools(){

		require('../DBConnection/connection.php');
		session_start();
		$final = array();
		//running query that would return all tools that are not currently reserved
		$allTools = $connection->query("SELECT t.Tool_id, t.Abbr_description
										FROM `Tool` t
										left join Reservation_line_item rli on t.Tool_id = rli.FK_Tool_id
										left join Reservation_header rh on rli.FK_Reservation_num = rh.Reservation_num
										where (CURDATE() not between Start_date and End_date or isnull(Start_date))
										and t.Sold_sw = 0;");

		//check if service_order already created....else show tool
		while($row = $allTools->fetch_assoc()){
			$tool_id = $row['Tool_id'];
			$service_orders = $connection->query("SELECT * from Service_order where FK_Tool_id = $tool_id AND CURDATE() < End_date");
			if($service_orders->num_rows == 0){
				$series['id'] = $row['Tool_id'];
				$series['description'] = $row['Abbr_description'];
				array_push($final, $series);
			}
			
		}

		print json_encode($final);

	}

	function getToolInfo(){
		require('../DBConnection/connection.php');
		session_start();
		$final = array();
		$tool_id = $_GET['tool_id'];

		$tool = $connection->query("SELECT * FROM Tool where Tool_id = $tool_id");
		$data = $tool->fetch_assoc();

		$sell_price = $data['Original_purchase_price'] - ($data['Original_purchase_price']/2);

		$series['Original_purchase_price'] = $data['Original_purchase_price'];
		$series['sell_price'] = $sell_price;

		array_push($final, $series);
		print json_encode($final);
	}

	function sellTool(){
		require('../DBConnection/connection.php');
		session_start();
		$tool_id = $_GET['tool_id'];

		$sql='UPDATE `Tool` 
				SET `Sold_sw` = "1"
				WHERE Tool_id = '.$tool_id.'';

		if($connection->query($sql))
			echo 'Success';
		else
			echo 'Failed';
	}

	function createServiceOrder(){
		require('../DBConnection/connection.php');
		session_start();
		$username = $_SESSION['login_id'];
		$tool_id = $_GET['tool_id'];
		$start_date = $_GET['start_date'];
		$end_date = $_GET['end_date'];
		$cost = $_GET['cost'];

		$result = $connection->query("SELECT MAX(Order_no) as order_no FROM service_order");
		if($result->num_rows == 0)
			$max_order = 0;
		else{
			$row = $result->fetch_assoc();
			$max_order = $row['order_no'];
		}
			// $max_order = $result->fetch_assoc()->[0];
		// $highest_id = $row[0];
		$highest_id = $max_order + 1;

		$sql = "INSERT INTO service_order 
						(FK_Tool_id,
		                  Start_date,
		                  Order_no,
					      End_date,				
					      Repair_cost,
					      FK_Emp_login_id)
				     VALUES ('$tool_id',
					     '$start_date',
					     '$highest_id',
					     '$end_date',
					     '$cost',
					     '$username')";	

		if($connection->query($sql))
			echo 'Success';
		else
			echo 'Failed';
	}

	function getReservationNums(){
		require('../DBConnection/connection.php');
		session_start();
		$final = array();

		$result = $connection->query("SELECT Reservation_num FROM Reservation_header where ISNULL(FK_Pickup_login_id)");
		if($result->num_rows == 0){
				$series['id'] = 0;
				$series['description'] = 'No Reservations Available';
				array_push($final, $series);
		}else{
			while($row = $result->fetch_assoc()){
					$series['id'] = $row['Reservation_num'];
					$series['description'] = $row['Reservation_num'];
					array_push($final, $series);				
			}
		}

		print json_encode($final);

	}

	function getPickedUpReservationNums(){
		require('../DBConnection/connection.php');
		session_start();
		$final = array();

		$result = $connection->query("SELECT Reservation_num FROM Reservation_header where !ISNULL(FK_Pickup_login_id) AND ISNULL(FK_Dropoff_login_id)");
		if($result->num_rows == 0){
				$series['id'] = 0;
				$series['description'] = 'No Reservations Available';
				array_push($final, $series);
		}else{
			while($row = $result->fetch_assoc()){
					$series['id'] = $row['Reservation_num'];
					$series['description'] = $row['Reservation_num'];
					array_push($final, $series);				
			}
		}

		print json_encode($final);
	}

	function getToolsRequired(){
		require('../DBConnection/connection.php');
		session_start();

		if(isset($_SESSION['pick_up_res_num']))
			$res_num = $_SESSION['pick_up_res_num'];
		else
			$res_num = $_GET['reservation_num'];

		$sql='SELECT `tool`.Tool_id, `tool`.Abbr_description 
		FROM `reservation_line_item` 
		JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
		WHERE FK_Reservation_num='.$res_num.'';
		$result=$connection->query($sql);
		$count = 1;
		echo '<table class="table table-hover">';
		echo '<thead><th></th><th>Tool ID</th><th>Abbr. Description</th></thead>';
		while($row = $result->fetch_assoc())
		{
			echo "<tr>";
			echo "<td>".$count."</td>";
			echo "<td>".$row['Tool_id']."</td>";
			echo "<td>".$row['Abbr_description']."</td>";
			echo "</tr>";
			$count++;
		}
		echo "</table>";
	}



	function getCostsPerReservtion(){
		require('../DBConnection/connection.php');
		session_start();
		$final = array();
		if(isset($_SESSION['pick_up_res_num']))
			$res_num = $_SESSION['pick_up_res_num'];
		else
			$res_num = $_GET['reservation_num'];

		/*GET DEPOSIT AMOUNT*/
		$sql='SELECT SUM(Deposit_amt) from (SELECT `tool`.Tool_id, `tool`.Rental_price, `tool`.Deposit_amt  
			  FROM `reservation_line_item` 
			  JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
			  WHERE FK_Reservation_num='.$res_num.') temp_table';
		$result=$connection->query($sql);
		$row = $result->fetch_assoc();
		$dep_req=$row['SUM(Deposit_amt)'];
		$dep_req='$'.$dep_req;

		#GET RENTAL COST
		$sql='SELECT SUM(Rental_price) from (SELECT `tool`.Tool_id, `tool`.Rental_price, `tool`.Deposit_amt 
			FROM `reservation_line_item` 
			JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
			WHERE FK_Reservation_num='.$res_num.') temp_table';
		$result=$connection->query($sql);
		$row = $result->fetch_assoc();
		$rental_cost=$row['SUM(Rental_price)']; #SUM RENTAL COSTS OF ALL TOOLS

		#GET NUMBER OF DAYS
		$sql='SELECT `rh`.Start_date, `rh`.End_date, rh.Credit_card_num, concat(c.First_name, " ",c.Last_name) as customer_name, concat(e.First_name, " ",e.Last_name) as employee_name
				FROM `reservation_header` rh
                join `Customer` c on rh.FK_Customer_login_id = c.Login_id
                join `Employee` e on rh.FK_Pickup_login_id = e.Login_id
				WHERE Reservation_num='.$res_num.';';
		$result=$connection->query($sql);
		while($row = $result->fetch_assoc()) 
		{
			$start_date=$row['Start_date'];
			$end_date=$row['End_date'];
			$clerk_name=$row['employee_name'];
			$customer_name=$row['customer_name'];
			$cc=$row['Credit_card_num'];
		}
		$end_date_temp=date_create($end_date);
		$start_date_temp=date_create($start_date);
		$num_days=date_diff($end_date_temp,$start_date_temp);
		$num_days=($num_days->format('%d')+1); #GET TOTAL NUMBER OF DAYS RESERVED (FENCE POST +1)
		$rental_cost=$rental_cost*$num_days;#MULT NUM DAYS BY RENTAL TOTAL
		$rental_cost='$'.$rental_cost.'.00';

		$series['deposit_required'] = $dep_req;
		$series['estimated_cost'] = $rental_cost;
		$series['reservation_num'] = $res_num;
		$series['clerk'] = $clerk_name;
		$series['customer'] = $customer_name;
		$series['cc'] = $cc;
		$series['start_date'] = $start_date;
		$series['end_date'] = $end_date;

		array_push($final, $series);

		print json_encode($final);

		unset($_SESSION['pick_up_res_num']);
	}

	function updatePickUp(){
		require('../DBConnection/connection.php');
		session_start();
		$username = $_SESSION['login_id'];
		$res_num = $_GET['reservation_num'];
		$cc = $_GET['cc'];
		$exp_date = $_GET['exp_date'];
		$sql='UPDATE `reservation_header` 
				SET `FK_Pickup_login_id` = "'.$username.'",
				`Credit_card_num` = "'.$cc.'",
				`Credit_card_exp_date` = "'.$exp_date.'"
				WHERE `reservation_header`.`Reservation_num` = '.$res_num.'';

		if($connection->query($sql)){
			echo 'Success';
			$_SESSION['pick_up_res_num'] = $res_num;
		}else
			echo 'Failed';
	}

	function updateDropoff(){
		require('../DBConnection/connection.php');
		session_start();
		$username = $_SESSION['login_id'];
		$res_num = $_GET['reservation_num'];
		$sql='UPDATE `reservation_header` 
				SET `FK_Dropoff_login_id` = "'.$username.'"
				WHERE `reservation_header`.`Reservation_num` = '.$res_num.'';

		$_SESSION['drop_off_res'] = $res_num;

		if($connection->query($sql))
			echo 'Success';
		else
			echo 'Failed';
	}

	function getDropOffReceipt(){
		require('../DBConnection/connection.php');
		session_start();
		$username = $_SESSION['login_id'];
		$res_num = $_SESSION['drop_off_res'];

		$final = array();

		/*GET DEPOSIT AMOUNT*/
		$sql='SELECT SUM(Deposit_amt) from (SELECT `tool`.Tool_id, `tool`.Rental_price, `tool`.Deposit_amt  
			  FROM `reservation_line_item` 
			  JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
			  WHERE FK_Reservation_num='.$res_num.') temp_table';
		$result=$connection->query($sql);
		$row = $result->fetch_assoc();
		$dep_req=$row['SUM(Deposit_amt)'];
		

		#GET RENTAL COST
		$sql='SELECT SUM(Rental_price) from (SELECT `tool`.Tool_id, `tool`.Rental_price, `tool`.Deposit_amt 
			FROM `reservation_line_item` 
			JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
			WHERE FK_Reservation_num='.$res_num.') temp_table';
		$result=$connection->query($sql);
		$row = $result->fetch_assoc();
		$rental_cost=$row['SUM(Rental_price)']; #SUM RENTAL COSTS OF ALL TOOLS

		#GET NUMBER OF DAYS
		$sql='SELECT `rh`.Start_date, `rh`.End_date, rh.Credit_card_num, concat(c.First_name, " ",c.Last_name) as customer_name, concat(e.First_name, " ",e.Last_name) as employee_name
				FROM `reservation_header` rh
                join `Customer` c on rh.FK_Customer_login_id = c.Login_id
                join `Employee` e on rh.FK_Dropoff_login_id = e.Login_id
				WHERE Reservation_num='.$res_num.';';
		$result=$connection->query($sql);
		while($row = $result->fetch_assoc()) 
		{
			$start_date=$row['Start_date'];
			$end_date=$row['End_date'];
			$clerk_name=$row['employee_name'];
			$customer_name=$row['customer_name'];
			$cc=$row['Credit_card_num'];
		}
		$end_date_temp=date_create($end_date);
		$start_date_temp=date_create($start_date);
		$num_days=date_diff($end_date_temp,$start_date_temp);
		$num_days=($num_days->format('%d')+1); #GET TOTAL NUMBER OF DAYS RESERVED (FENCE POST +1)
		$rental_cost=$rental_cost*$num_days;#MULT NUM DAYS BY RENTAL TOTAL
		$total = $rental_cost - $dep_req;
		$rental_cost='$'.$rental_cost.'.00';
		$dep_req='$'.$dep_req;

		$series['reservation_num'] = $res_num;
		$series['clerk'] = $clerk_name;
		$series['customer'] = $customer_name;
		$series['cc'] = $cc;
		$series['start_date'] = $start_date;
		$series['end_date'] = $end_date;
		$series['rental_price'] = $rental_cost;
		$series['deposit'] = $dep_req;
		$series['total'] = '$'.$total.'.00';

		array_push($final, $series);
		print json_encode($final);

		unset($_SESSION['drop_off_res']);

	}

	function Logout(){
		session_start();
		if(session_destroy())
			echo 'Success';
		else
			echo 'Failed';

	}

	function checkUserType(){
		require('../DBConnection/connection.php');
		session_start();
		$username = $_SESSION['login_id'];
		$customer = $connection->query("SELECT Login_id, Password FROM `Customer` WHERE Login_id='$username'");
		if($customer->num_rows > 0)
			echo "http://localhost/handyman_tools/CustomerMainMenu.html";

		$employee = $connection->query("SELECT Login_id, Password FROM `Employee` WHERE Login_id='$username'");
		if($employee->num_rows > 0)
			echo "http://localhost/handyman_tools/sebastian/ClerkMainMenu.html";
	}


	function checkSession(){
		session_start();
		require('../DBConnection/connection.php');
		if(isset($_SESSION['login_id']))
			echo 'True';
		else
			echo 'False';
	}
}


if(isset($_GET['method'])){
   	$functions = new functions;
   	$method = $_GET['method'];
   	switch ($method) {
		case 'getCustProfile':
			$functions->getCustProfile();
			break;
		case 'createNewCustomer':
			$functions->createNewCustomer();
			break;
		case 'createNewEmp':
			$functions->createNewEmp();
			break;
   		case 'addNewTool':
   			$functions->insertNewTool();
   			break;
   		case 'login':
   			$functions->Login();
   			break;
   		case 'checkSession':
   			$functions->checkSession();
   			break;
   		case 'checkUserType':
   			$functions->checkUserType();
   			break;
   		case 'logout':
   			$functions->Logout();
   			break;
   		case 'getTools':
   			$functions->getTools();
   			break;
   		case 'createServiceOrder':
   			$functions->createServiceOrder();
   			break;
   		case 'getReservationNums':
   			$functions->getReservationNums();
   			break;
   		case 'getToolsRequired':
   			$functions->getToolsRequired();
   			break;
   		case 'getCostsPerReservtion':
   			$functions->getCostsPerReservtion();
   			break;
   		case 'viewDetails':
   			$functions->viewDetails();
   			break;

   		case 'updatePickUp':
   			$functions->updatePickUp();
   			break;
   		case 'getPickedUpReservationNums':
   			$functions->getPickedUpReservationNums();
   			break;
   		case 'updateDropoff':
   			$functions->updateDropoff();
   			break;
   		case 'getDropOffReceipt':
   			$functions->getDropOffReceipt();
   			break;
   		case 'getToolInfo':
   			$functions->getToolInfo();
   			break;
   		case 'getAvailableTools':
   			$functions->getAvailableTools();
   			break;
   		case 'sellTool':
   			$functions->sellTool();
   			break;
   		case 'inventoryReport':
   			$functions->inventoryReport();
   			break;
   		case 'customerReport':
   			$functions->customerReport();
   			break;
   		case 'employeeReport':
   			$functions->employeeReport();
   			break;
                case 'getCalcTotal':
		     $functions->getCalcTotal();
		    break;
			
	        case 'displayReservationData':
		     $functions->displayReservationData();
		     break;
			 
				 
            case 'reservationFinal':
		     $functions->reservationFinal();
		     break;
             
			case 'displayFinalReservationData':
		     $functions->displayFinalReservationData();
		     break;
			 		

   		default:
   			# code...
   			break;
   	}
   	//$functions->$_GET['method']();
}

?>