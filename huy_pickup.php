<?php
session_start();
$_SESSION['res_num'];

#read in res num
#read in clerk
#doing anything w/ expiration date?

require('DBConnection/connection.php');

$res_num='1253'; #need to make dynamic
$_SESSION['res_num']=$res_num;

$clerk='harry@fbi.gov'; #need to make dynamic
$_SESSION['clerk']=$clerk;

/*GET DEPOSIT AMOUNT*/
$sql='SELECT SUM(Deposit_amt) from (SELECT `tool`.Tool_id, `tool`.Rental_price, `tool`.Deposit_amt  
	  FROM `reservation_line_item` 
	  JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
	  WHERE FK_Reservation_num='.$res_num.') temp_table';
$result=$connection->query($sql);
$row = $result->fetch_assoc();
$dep_req=$row['SUM(Deposit_amt)'];
$dep_req='$'.$dep_req;

#GET RENTAL COST
$sql='SELECT SUM(Rental_price) from (SELECT `tool`.Tool_id, `tool`.Rental_price, `tool`.Deposit_amt 
	FROM `reservation_line_item` 
	JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
	WHERE FK_Reservation_num='.$res_num.') temp_table';
$result=$connection->query($sql);
$row = $result->fetch_assoc();
$rental_cost=$row['SUM(Rental_price)']; #SUM RENTAL COSTS OF ALL TOOLS

#GET NUMBER OF DAYS
$sql='SELECT `reservation_header`.Start_date, `reservation_header`.End_date
		FROM `reservation_header`
		WHERE Reservation_num='.$res_num.';';
$result=$connection->query($sql);
while($row = $result->fetch_assoc()) 
{
	$start_date=$row['Start_date'];
	$end_date=$row['End_Fodate'];
}
$end_date=date_create($end_date);
$start_date=date_create($start_date);
$num_days=date_diff($end_date,$start_date);
$num_days=($num_days->format('%d')+1); #GET TOTAL NUMBER OF DAYS RESERVED (FENCE POST +1)
$rental_cost=$rental_cost*$num_days;#MULT NUM DAYS BY RENTAL TOTAL
$rental_cost='$'.$rental_cost.'.00';			
?>

<html>
<title>Pick Up</title>
<style type="text/css">
    .bottom_container 
	{
        	width: 500px;
        	clear: both;
    }
    .bottom_container input 
	{
        width: 50%;
        clear: both;
		position: absolute;
		left: 170px;
    }
</style>
<body>
	<b>Reservation Number: </b>
	<?php echo $res_num; ?>
	<br><br>
	<b>Tools Required: </b>
	<?php 
		$sql='SELECT `tool`.Tool_id, `tool`.Abbr_description 
		FROM `reservation_line_item` 
		JOIN `tool` ON `tool`.Tool_id=`reservation_line_item`.FK_Tool_id 
		WHERE FK_Reservation_num='.$res_num.'';
		$result=$connection->query($sql);
		echo "<table>";
		while($row = $result->fetch_assoc())
		{
			echo "<tr>";
			echo "<td>".$row['Tool_id']."</td>";
			echo "<td></td><td></td><td></td><td></td>";
			echo "<td>".$row['Abbr_description']."</td>";
			echo "</tr>";
		}
		echo "</table>";
	?>
	<br>
	<b>Deposits Required: </b>
	<?php echo $dep_req; ?>
	<br><br>
	<b>Estimated Cost: </b>
	<?php echo $rental_cost; ?>
	<br><br>
	<hr><br>
	
	<form id="viewTool" name="viewTool" method="post" onsubmit="">
	<!-- <form action="huy_pickup_tool_detail.php" method="post"> -->
		<b>Tool ID</b> <input type="text" name="tID">
		<input type="button" onclick="submitViewDetails('huy_pickup_tool_detail.php')" value="View Details"/>
		<!-- <input type="submit" name="View Details" value="View Details"> -->
		<br><br>
		<hr>
	</form>
	<div class="bottom_container">
	<form id="completeForm" name="completeForm" method="post" onsubmit="">
	<!-- <form action="huy_rental_contract.html" method="post"> -->
		<b>Credit Card Number</b>
		<input type="text" name="ccNum"><br>
		<b>Expiration Date</b>
		<input type="text" name="expDate"><br><br>
	</form>
	</div>
	<input type="button" onclick="getRentalContract('huy_rental_contract.php')" value="Complete Form"/>
	<!-- <input type="submit" onclick="completeForm('huy_rental_contract.html')" value="Complete Form" /> -->
	
	<script type="text/javascript">
    function submitViewDetails(action)
    {
        document.getElementById('viewTool').action = action;
        document.getElementById('viewTool').submit();
    }
	
    function getRentalContract(action)
    {
		<?php #UPDATE DB WITH CLERK DOING CHECKOUT
			$sql='UPDATE `reservation_header` 
				SET `FK_Pickup_login_id` = "'.$clerk.'"'.' 
				WHERE `reservation_header`.`Reservation_num` = '.$res_num.'';
				
			$connection->query($sql);
		?>
		
        document.getElementById('completeForm').action = action;
        document.getElementById('completeForm').submit();
    }
	</script>
	
</body>
</html>
