CREATE DATABASE `handy_tool` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `handy_tool`;

CREATE TABLE Customer (
    Login_id VARCHAR(50) NOT NULL,
    Password VARCHAR(50) NOT NULL,
    First_name VARCHAR(25) NOT NULL,
    Last_name VARCHAR(35) NOT NULL,
    Home_phone VARCHAR(10) NOT NULL,
    Work_phone VARCHAR(10) NOT NULL,
    Street VARCHAR(50) NOT NULL,
    City VARCHAR(5) NOT NULL,
    State VARCHAR(40) NOT NULL,
    Zip VARCHAR(20) NOT NULL,
    PRIMARY KEY (Login_id)
);


CREATE TABLE Employee (
    Login_id VARCHAR(50) NOT NULL,
    Password VARCHAR(50) NOT NULL,
    First_name VARCHAR(25) NOT NULL,
    Last_name VARCHAR(35) NOT NULL,
    Ssn VARCHAR(15) NOT NULL,
    PRIMARY KEY (Login_id)
);


DROP TABLE IF EXISTS `reservation_header`;
CREATE TABLE IF NOT EXISTS `reservation_header` (
  `Reservation_num` int(11) NOT NULL AUTO_INCREMENT,
  `Start_date` date NOT NULL,
  `End_date` date NOT NULL,
  `Credit_card_num` varchar(20) DEFAULT NULL,
  `Credit_card_exp_date` char(6) DEFAULT NULL,
  `FK_Pickup_login_id` varchar(50) DEFAULT NULL,
  `FK_Dropoff_login_id` varchar(50) DEFAULT NULL,
  `FK_Customer_login_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Reservation_num`),
  KEY `FK_Pickup_login_id` (`FK_Pickup_login_id`),
  KEY `FK_Dropoff_login_id` (`FK_Dropoff_login_id`),
  KEY `FK_Customer_login_id` (`FK_Customer_login_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5734 DEFAULT CHARSET=latin1;

/* not sure if reservation_num is auto increment */
-- ALTER TABLE `handy_tool`.`Reservation_header` 
-- CHANGE COLUMN `Reservation_num` `Reservation_num` INT(11) NOT NULL AUTO_INCREMENT COMMENT '' ;


CREATE TABLE Tool (
    Tool_id INT NOT NULL,
    Abbr_description VARCHAR(50) NOT NULL,
    Long_description VARCHAR(50) NOT NULL,
    Sold_sw CHAR(1) NOT NULL,
    Rental_price DECIMAL(6 , 2 ) NOT NULL CHECK (Rental_price > 0),
    Deposit_amt DECIMAL(6 , 2 ) NOT NULL CHECK (Deposit_amt > 0),
    Original_purchase_price DECIMAL(8 , 2 ) NOT NULL CHECK (Original_purchase_price > 0),
    Category VARCHAR(15) NOT NULL,
    PRIMARY KEY (Tool_id)
);


ALTER TABLE `handy_tool`.`Tool` 
CHANGE COLUMN `Tool_id` `Tool_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '' ;

CREATE TABLE Power_tool (
    FK_Power_tool_id INT NOT NULL,
    Power_source VARCHAR(20) NULL,
    PRIMARY KEY (FK_Power_tool_id),
    FOREIGN KEY (FK_Power_tool_id)
        REFERENCES Tool (Tool_id)
);


CREATE TABLE Construction_tool (
    FK_Construct_tool_id INT NOT NULL,
    Size VARCHAR(20) NULL,
    PRIMARY KEY (FK_Construct_tool_id),
    FOREIGN KEY (FK_Construct_tool_id)
        REFERENCES Tool (Tool_id)
);

CREATE TABLE Hand_tool (
    FK_Hand_tool_id INT NOT NULL,
    Weight VARCHAR(20) NULL,
    PRIMARY KEY (FK_Hand_tool_id),
    FOREIGN KEY (FK_Hand_tool_id)
        REFERENCES Tool (Tool_id)
);


CREATE TABLE Reservation_line_item (
    FK_Reservation_num INT NOT NULL,
    Sequence_num INT NOT NULL,
    FK_Tool_id INT NOT NULL,
    PRIMARY KEY (FK_Reservation_num , Sequence_num , FK_Tool_id),
    FOREIGN KEY (FK_Reservation_num)
        REFERENCES Reservation_header (Reservation_num),
    FOREIGN KEY (FK_Tool_id)
        REFERENCES Tool (Tool_id)
);

CREATE TABLE Accessory (
    FK_Acc_pow_tool_id INT NOT NULL,
    Name VARCHAR(50) NOT NULL,
    Quantity INT,
    PRIMARY KEY (FK_Acc_pow_tool_id , Name),
    FOREIGN KEY (FK_Acc_pow_tool_id)
        REFERENCES Power_tool (FK_Power_tool_id)
);



CREATE TABLE Service_order (
    FK_Tool_id INT NOT NULL,
    Start_date DATE NOT NULL,
    Order_no INT NOT NULL,
    End_date DATE NOT NULL,
    Repair_cost DECIMAL(7 , 2 ) NOT NULL,
    FK_Emp_login_id VARCHAR(50) NOT NULL,
    PRIMARY KEY (FK_Tool_id , Start_date , Order_no),
    FOREIGN KEY (FK_Tool_id)
        REFERENCES Tool (Tool_id),
    FOREIGN KEY (FK_Emp_login_id)
        REFERENCES Employee (Login_id)
);


INSERT INTO `handy_tool`.`Employee` (`Login_id`, `Password`, `First_name`, `Last_name`, `Ssn`) VALUES ('rmartinez6@gatech.edu', 'testing', 'Ricardo', 'Martinez', '111-11-1111');
INSERT INTO `handy_tool`.`Employee` (`Login_id`, `Password`, `First_name`, `Last_name`, `Ssn`) VALUES ('vg3@gatech.edu', 'testing', 'Vijay', 'Narasimhamurthy', '222-22-2222');

/* Huy: Customer table restricts varchar for phone and city; we might need to change table implementation. Temp fix below.
INSERT INTO `handy_tool`.`Customer` (`Login_id`, `Password`, `First_name`, `Last_name`, `Home_phone`, `Work_phone`, `Street`, `City`, `State`, `Zip`) VALUES ('sprice36@gatech.edu', 'testing', 'Sebastian', 'Price', '555-555-5555', '777-777-7777', '10 Georgia Street ', 'Atlanta', 'Georgia', '12345');
INSERT INTO `handy_tool`.`Customer` (`Login_id`, `Password`, `First_name`, `Last_name`, `Home_phone`, `Work_phone`, `Street`, `City`, `State`, `Zip`) VALUES ('htran30@gatech.edu', 'testing', 'Huy', 'Tran', '444-444-4444', '333-333-3333', '11 Georgia Tech Way', 'Atlanta', 'Georgia', '12345');
*/
INSERT INTO `handy_tool`.`Customer` (`Login_id`, `Password`, `First_name`, `Last_name`, `Home_phone`, `Work_phone`, `Street`, `City`, `State`, `Zip`) VALUES ('sprice36@gatech.edu', 'testing', 'Sebastian', 'Price', '5555555555', '7777777777', '10 Georgia Street ', 'ATL', 'Georgia', '12345');
INSERT INTO `handy_tool`.`Customer` (`Login_id`, `Password`, `First_name`, `Last_name`, `Home_phone`, `Work_phone`, `Street`, `City`, `State`, `Zip`) VALUES ('htran30@gatech.edu', 'testing', 'Huy', 'Tran', '4444444444', '3333333333', '11 Georgia Tech Way', 'ATL', 'Georgia', '12345');

INSERT INTO `handy_tool`.`Tool` (`Abbr_description`, `Long_description`, `Sold_sw`, `Rental_price`, `Deposit_amt`, `Original_purchase_price`, `Category`) VALUES ('Paint Sprayer', 'M300 Paint Sprayer Pro', '0', '12.00', '3.00', '50.00', 'H');
INSERT INTO `handy_tool`.`Tool` (`Abbr_description`, `Long_description`, `Sold_sw`, `Rental_price`, `Deposit_amt`, `Original_purchase_price`, `Category`) VALUES ('Jack Hammer', 'The Ultimate Jack Hammer', '0', '6.00', '2.00', '30.00', 'C');
INSERT INTO `handy_tool`.`Tool` (`Abbr_description`, `Long_description`, `Sold_sw`, `Rental_price`, `Deposit_amt`, `Original_purchase_price`, `Category`) VALUES ('Wall Driller', 'M1600 Wall Driller', '0', '25.00', '7.00', '100.00', 'P');


INSERT INTO `handy_tool`.`Hand_tool` (`FK_Hand_tool_id`, `Weight`) VALUES ('1', '15 lbs');
INSERT INTO `handy_tool`.`Power_tool` (`FK_Power_tool_id`, `Power_source`) VALUES ('3', 'Electric Outlet');
INSERT INTO `handy_tool`.`construction_tool` (`FK_Construct_tool_id`, `Size`) VALUES ('2', '1 ft');

INSERT INTO `handy_tool`.`Reservation_header` (`Reservation_num`, `Start_date`, `End_date`) VALUES ('1253', '2016-03-25', '2016-04-04');
INSERT INTO `handy_tool`.`Reservation_header` (`Reservation_num`, `Start_date`, `End_date`) VALUES ('5731', '2016-03-25', '2016-04-15');


INSERT INTO `handy_tool`.`Reservation_line_item` (`FK_Reservation_num`, `Sequence_num`, `FK_Tool_id`) VALUES ('1253', '1', '1');
INSERT INTO `handy_tool`.`Reservation_line_item` (`FK_Reservation_num`, `Sequence_num`, `FK_Tool_id`) VALUES ('5731', '2', '3');

INSERT INTO `handy_tool`.`Service_order` (`FK_Tool_id`, `Start_date`, `Order_no`, `End_date`, `Repair_cost`, `FK_Emp_login_id`) VALUES ('2', '2016-03-25', '1', '2016-04-01', '34.99', 'rmartinez6@gatech.edu');

INSERT INTO `handy_tool`.`Accessory` (`FK_Acc_pow_tool_id`, `Name`, `Quantity`) VALUES ('3', 'Electric Adapter', '1');