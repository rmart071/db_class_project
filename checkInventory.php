<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Check Availability</title>

    <!-- Bootstrap core CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="resources/css/offcanvas.css" rel="stylesheet">
  </head>
<body>
  <nav class="navbar navbar-fixed-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Handyman Tool</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->

<div class="container col-lg-6 col-lg-offset-3 col-md-4 col-md-offset-4">
<h1>Tool Availabilty</h1>
<table class="table table-hover" >
	<thead>
		<th>Tool ID</th>
		<th>Abbr. Description</th>
		<th>Deposit ($)</th>
		<th>Price/Day ($)</th>
	</thead>
	<tbody>


<?php
require('DBConnection/connection.php');
require_once 'PHPFunctions/functions.php';
$functions = new functions();

// echo $functions->viewDetails(1);

$tool_type = $_POST['tool_type'];
$Start_date = $_POST['start_date'];
$End_date = $_POST['end_date'];

//get all the tools pertaining to selected category

$tools = $connection->query("SELECT * from Tool where Category = '$tool_type'");

//if no reservation is found show data
//if reservation is outside of date show data
//if item is not in service order show data


//keep track of id
$id_array = array();

while($tool = $tools->fetch_assoc()){
	// print_r($tool);
	$tool_id = $tool['Tool_id'];
	//check if reservation or service order is not found
	$hasBeenReserved = $connection->query("SELECT * from Reservation_line_item where FK_Tool_id = $tool_id");
	$hasBeenServiced = $connection->query("SELECT * from Service_order where FK_Tool_id = $tool_id");
	if($hasBeenReserved->num_rows == 0 && $hasBeenServiced->num_rows == 0){
		$series['id'] = $tool_id;
		array_push($id_array, $series);
	}else{
		//check reservation header
		switch ($tool_type) {
			case 'H':
				$sql = "SELECT * from Hand_tool ht 
							left join Reservation_line_item rli on ht.FK_Hand_tool_id = rli.FK_Tool_id
							left join Reservation_header rh on rli.FK_Reservation_num = rh.Reservation_num
							WHERE  NOT (('$Start_date' >= rh.Start_date AND '$Start_date' <= rh.End_date)
							OR ('$End_date' >= rh.Start_date AND '$End_date' <= rh.End_date))";
				break;
			case 'C':
				$sql = "SELECT * from Construction_tool ct 
							left join Reservation_line_item rli on ct.FK_Construct_tool_id = rli.FK_Tool_id
							left join Reservation_header rh on rli.FK_Reservation_num = rh.Reservation_num
							WHERE  NOT (('$Start_date' >= rh.Start_date AND '$Start_date' <= rh.End_date)
							OR ('$End_date' >= rh.Start_date AND '$End_date' <= rh.End_date))";
				break;
			case 'P':
				$sql = "SELECT * from Power_tool pt 
							left join Reservation_line_item rli on pt.FK_Power_tool_id = rli.FK_Tool_id
							left join Reservation_header rh on rli.FK_Reservation_num = rh.Reservation_num
							WHERE  NOT (('$Start_date' >= rh.Start_date AND '$Start_date' <= rh.End_date)
							OR ('$End_date' >= rh.Start_date AND '$End_date' <= rh.End_date))";
				break;
			
			default:
				echo 'Tool type unknown!';
				break;
		}

		$reservation = $connection->query($sql);

		$serviceOrder = $connection->query("SELECT * from service_order WHERE FK_Tool_id = $tool_id AND  NOT (('$Start_date' >= Start_date AND '$Start_date' <= End_date)
							OR ('$End_date' >= Start_date AND '$End_date' <= End_date))");


		if($serviceOrder->num_rows > 0 || $reservation->num_rows > 0){
			$series['id'] = $tool_id;
			array_push($id_array, $series);
		}
	}
	

}


$result = $connection->query($sql);
foreach ($id_array as $row) {
	$tool_id = $row['id'];
	$toolQuery = "SELECT * from Tool where Tool_id = $tool_id";
	$toolData = $connection->query($toolQuery);
	$row2 = $toolData->fetch_assoc();
	$Description = $row2['Abbr_description'];
	$Deposit = $row2['Deposit_amt'];
	$Rental_price = $row2['Rental_price'];
	// $ $row2['Tool_id'];
	echo '<tr>';
	echo '<td>'.$tool_id.'</td>';
	echo '<td>'.$Description.'</td>';
	echo '<td>'.$Deposit.'</td>';
	echo '<td>'.$Rental_price.'</td>';
	echo '</tr>';
}

?>
</tbody>
</table>

<br>
<form id="myForm" class="text-left">
	<label class="form-control-label text-center" for="inputDanger1" id="inputDanger1" style="color:#a94442; display:none">Please Enter ID</label>
	<label class="form-control-label text-center" for="inputDanger1" id="inputDanger2" style="color:#a94442; display:none">Enter Valid ID</label>
		<div id="validation_form" class="form-group  has-feedback form-inline">

  			<input type="text" class="form-control" id="detail_input" name="view_details" placeholder="Part #"> <button type="button" class="btn btn-success" id="view_details">View Details</button><br>

  		</div>

<!--   		<div class="form-group has-error has-feedback">
      <label class="col-sm-2 control-label" for="inputError">Input with error and glyphicon</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="inputError">
        
      </div>
    </div> -->
  	<hr>
 </form>
 </div>
<div class="container col-lg-6 col-lg-offset-3 col-md-4 col-md-offset-4">
<form action="checkInventory.html" method="post">
	<button class="btn btn-primary">Back to Main</button>
</form>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="resources/js/bootstrap.min.js"></script>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title" id="myModalLabel">Tool Details</h2>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

	$('#view_details').click(function(){
		var id_present = false; 
		var id = 0;
		//check if ID is present
		$("table tr td:nth-child(1)").each(function () {
			if($('#detail_input').val() == $(this).text()){
				id_present = true; 
				id = $('#detail_input').val();
			}
		});

		//check if input has been placed
		if($('#detail_input').val().length == 0){
			$('#inputDanger1').show();
			$('#inputDanger2').hide();
			$('#validation_form').addClass('has-error')
		}else if(!id_present){
			$('#inputDanger1').hide();
			$('#inputDanger2').show();
			$('#validation_form').addClass('has-error')
		}else{
			$('#inputDanger1').hide();
			$('#inputDanger2').hide();
			$('#validation_form').removeClass('has-error')
			$.ajax({
				'url' : 'PHPFunctions/functions.php?method=viewDetails',
				'data': {'toolID': id},
				'success': function(data){
					$('.modal-body').html(data)
				},
				'error':function(data){
					console.log(data)
				}
			})
			$('#myModal').modal('show'); 
		}

	})

	$('#myForm').submit(function(event){

  // prevent default browser behaviour
  		event.preventDefault();
  		var id_present = false; 
		var id = 0;
		//check if ID is present
		$("table tr td:nth-child(1)").each(function () {
			if($('#detail_input').val() == $(this).text()){
				id_present = true; 
				id = $('#detail_input').val();
			}
		});

		//check if input has been placed
		if($('#detail_input').val().length == 0){
			$('#inputDanger1').show();
			$('#inputDanger2').hide();
			$('#validation_form').addClass('has-error')
		}else if(!id_present){
			$('#inputDanger1').hide();
			$('#inputDanger2').show();
			$('#validation_form').addClass('has-error')
		}else{
			$('#inputDanger1').hide();
			$('#inputDanger2').hide();
			$('#validation_form').removeClass('has-error')
			$.ajax({
				'url' : 'PHPFunctions/functions.php?method=viewDetails',
				'data': {'toolID': id},
				'success': function(data){
					$('.modal-body').html(data)
				},
				'error':function(data){
					console.log(data)
				}
			})
			$('#myModal').modal('show'); 
		}

  //do stuff with your form here

});
</script>

</html>