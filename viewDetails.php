<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

   <title>Tool Detail</title>

    <!-- Bootstrap core CSS -->
    <link href="resources/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="resources/css/offcanvas.css" rel="stylesheet">
  </head>

<body>
	<nav class="navbar navbar-fixed-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Handyman Tool</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!-- /.nav-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
    <div class="container col-lg-6 col-lg-offset-3 col-md-4 col-md-offset-4">
	<h1>Tool Details</h1>
<?php
require('DBConnection/connection.php');

if($_POST['view_details']){
	$tool_id = $_POST['view_details'];
	$sql = "SELECT * from Tool where Tool_id = $tool_id";

	$result = $connection->query($sql)->fetch_assoc();
	if($result['Category'] == 'P')
		$type = 'Power Tool';
	elseif($result['Category'] == 'C')
		$type = 'Construction Tool';
	elseif($result['Category'] == 'H')
		$type = 'Hand Tool';
	else
		$type = 'Unknown Type';

	echo '<table class="table table-hover">';
	echo '<tbody>
		<tr>
			<td>Abbreviated Description: </td><td>'.$result['Abbr_description'].'</td>
		</tr>
		<tr>
			<td>Full Description: </td><td>'.$result['Long_description'].'</td>
		</tr>
		<tr>
			<td>Rental Price: </td><td>$'.$result['Rental_price'].'</td>
		</tr>
		<tr>
			<td>Deposit Amount: </td><td>$'.$result['Deposit_amt'].'</td>
		</tr>
		<tr>
			<td>Original Purchase Price: </td><td>$'.$result['Original_purchase_price'].'</td>
		</tr>
		<tr>
			<td>Tool Type: </td><td>'.$type.'</td>
		</tr>
			</tbody>';
	echo '</table>';


	if($result['Category'] == 'P'){
		$accessory_sql = "SELECT * FROM Accessory";
		

		echo '<hr/><h2>Tool Accessories</h2>';
		echo '<table class="table table-hover" >';
		echo '<thead>
				<th>Name</th>
				<th>Quantity</th>
		</thead>';
		echo '<tbody>';
		$result = $connection->query($accessory_sql);
		while($row = $result->fetch_assoc()){
		echo '	
			<tr>
				<td>'.$row['Name'].'</td>
				<td>'.$row['Quantity'].'</td>
			</tr>';
		}


		echo '</tbody>
			</table>';

	}


}else{
	echo 'No tool was selected';
}


?>
<br>
<form action="checkInventory.html" method="post">
	<!-- <input type="Submit" name="go_back" value="Go Back"> -->
	<button name="go_back" class="btn btn-primary">Go back</button>
</form>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
</body>
</html>